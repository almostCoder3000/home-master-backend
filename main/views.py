from django.shortcuts import render
from django.core import serializers
import json
from django.http import JsonResponse
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from .models import Questionare, Review, Malfunction, UserDetails, MalfunctionType, EQUIPMENT_TYPES
from django.views.decorators.http import require_GET, require_POST
from datetime import datetime


@csrf_exempt
@require_GET
def get_reviews(req):
    return JsonResponse({"data": serializeToJson(Review.objects.filter(isPublished=True))})


@csrf_exempt
@require_POST
def send_review(req):
    data = json.loads(req.body)
    addresser = data.get('addresser')
    city = data.get('city')
    text = data.get('text')

    if addresser and city and text:
        review = Review(
            addresser=addresser,
            city=city,
            text=text,
            date=datetime.now(),
            isPublished=False
        )
        review.save()
    else:
        return JsonResponse({"data": "Not params"})
    return JsonResponse({"data": "ok"})


@csrf_exempt
@require_POST
def leave_comment(req):
    data = json.loads(req.body)
    comment = Questionare(
        text=data.get('text')
    )
    comment.save()
    return JsonResponse({"data": "ok"})


@csrf_exempt
@require_POST
def leave_details(req):
    data = json.loads(req.body)
    name = data.get('name')
    phone_number = data.get('phone_number')
    email = data.get('email')
    malfunction_type = data.get('malfunction_type')
    equipment_type = data.get('equipment_type')
    isQuestionare = data.get('isQuestionare')

    if name and phone_number:
        details = UserDetails(
            name=name,
            phone_number=phone_number
        )
        mail_message = "От: {0}; \nНомер телефона: {1}; \n".format(
            name, phone_number)

        if isQuestionare:
            details.isQuestionare = isQuestionare
            mail_message += "Использование опросника: да; \n"

        if email and malfunction_type and equipment_type:
            malfunction_type = MalfunctionType.objects.filter(
                pk=malfunction_type).first()
            details.email = email
            details.malfunction_type = malfunction_type
            details.equipment_type = equipment_type
            mail_message += "Почта: {0}; \nНеисправность: {1}; \nТип техники: {2};".format(
                email, malfunction_type.title, list(
                    filter(lambda e: e[0] == equipment_type, EQUIPMENT_TYPES))[0][1]
            )
        try:

            send_mail(
                'Заявка от {0}'.format(name),
                mail_message,
                'homemaster.official@gmail.com',
                ['homemaster.official@gmail.com'],
                fail_silently=False,
            )
        except Exception as inst:
            return JsonResponse(serializeToJson({"data": inst}))

    else:
        return JsonResponse({"data": "not params"})
    details.save()
    return JsonResponse({"data": "ok"})


@require_GET
def get_malfunctions(req):
    eq = req.GET.get('equipment')
    malfunctions = []
    if eq:
        if eq == 'all':
            for eq_type in EQUIPMENT_TYPES:
                mal_types = MalfunctionType.objects.filter(
                    equipment_type__contains=eq_type[0])
                malfunctions.append({
                    "title": eq_type[1],
                    "eq_type": eq_type[0],
                    "cells": serializeToJson(
                        Malfunction.objects.filter(
                            type__in=[mt.pk for mt in mal_types])
                    )[:5]
                })
        else:
            eq_type = list(filter(lambda e: e[0] == eq, EQUIPMENT_TYPES))
            if len(eq_type) > 0:
                mal_types = MalfunctionType.objects.filter(
                    equipment_type__contains=eq_type[0][0])

                for mal_type in mal_types:
                    if eq_type[0][0] in mal_type.equipment_type:
                        malfunctions.append({
                            "pk": mal_type.pk,
                            "title": mal_type.title,
                            "icon_name": mal_type.icon_name,
                            "problems": serializeToJson(Malfunction.objects.filter(type=mal_type.pk))
                        })

            else:
                return JsonResponse({"error": "eq_type doesn't exists"})

    else:
        return JsonResponse({"error": "not params"})

    return JsonResponse({"data": malfunctions})


def serializeToJson(obj):
    obj = json.loads(serializers.serialize('json', obj))
    new_obj = []

    for el in obj:
        pk = el['pk']
        el = el['fields']
        el['pk'] = pk
        new_obj.append(el)

    return new_obj


def valuesQuerySetToDict(vqs):
    return [item for item in vqs]


def normalizeMalfunctions(malfunctions):
    # for ml in malfunctions:

    return malfunctions
