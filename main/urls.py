from . import views
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('reviews/get', views.get_reviews),
    path('reviews/send', views.send_review),
    path('comment/set', views.leave_comment),
    path('details/set', views.leave_details),
    path('malfunctions/get', views.get_malfunctions),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)